;Displays a box

;----------------------------------------------------------------------- STACK SEGMENT
MyStack SEGMENT STACK              

        DW 256 DUP (?)             ; At least a small stack is needed for interrupt
                                   ; handling.

MyStack ENDS                       ; End Segment

;------------------------------------------------------------------------ DATA SEGMENT
MyData SEGMENT
    
    	randomNumber DW 0
    	multiplyByValue DW 1361
    	addValue DW 569
    	snowPercentage DW 50
    	snowSpeed DW 50
    	updateTime DW 0
    	startPosition DW 1020
    	rightColumn DB 0
    	leftColumn DB 0
    	topRow DB 0
    	bottomRow DB, 0
    	borderSize DB 0
    	foregroundPos DB 0	
    	backgroundPos DB 2
    	colorArray DB 01,02,04,07
    	counter DW 0
    	timeCounter DW 0
    	foregroundColor DB 7
    	backgroundColor DB 4
    	boxHeight DB 8
    	boxWidth DB 32
    	singleLine DB 0DAh,0BFh,0C0h,0D9h,0C4h,0B3h
    	doubleLine DB 0C9h,0BBh,0C8h,0BCh,0CDh,0BAh
    	snowScreen DW 2000 DUP (0720h)
    	UpperLeft EQU 0
    	UpperRight EQU 1
    	LowerLeft EQU 2
    	LowerRight EQU 3
    	HorizontalLine EQU 4
    	VerticalLine EQU 5
    	VideoMem EQU 0B800h           			; Segment for video RAM.

MyData ENDS                        			; End Segment

;------------------------------------------------------------------------ CODE SEGMENT

MyCode SEGMENT
        ASSUME CS:MyCode, DS:MyData   		 	; Tell the assembler what segments the segment 
                                      		 	; registers refer to in the following code.

;------------------------------------------------------------------------- MAIN

Main	  PROC                        			; User Transfer Address
	
	MOV     AX, MyData             			; Make DS address our data segment.  Two statements
	MOV     DS, AX                 			; are required because it is illegal to move
                                       			; immediate data directly into a segment register.
                                       			; This is essential if your program refers to any
                                       			; of symbols declared with DB, DW, or DD in the
                                       			; data segment of your program.

	MOV     AX, VideoMem           			; Make ES segment register address video memory.
	MOV     ES, AX

	CALL ClearScreen
	CALL DrawBox
	
	MOV AH, 0					;Get timer ticks
	INT 1Ah
	MOV randomNumber, DX
	MOV AX, snowSpeed
	ADD DX, AX
	MOV updateTime,DX
	MOV AX, 0
	CALL GetEdges					;Sets the left and right column positions, and top and bottom row positions
	
   	MainLoop:
   	
 		MOV AH, 11h
 		INT 16h
 		JZ BackgroundProcess			;Is there a key pressed?
		CALL ProcessKey
		CALL DrawBox		      		; Draws box with current specifications
							; Saves SI, AX, BX, CX, DX,  and DI
		BackgroundProcess:
			CALL Count
			CALL CheckTime
			JC NoUpdate
				CALL UpdateSnow
				CALL AddNewRow
				CALL GetBoxView
			NoUpdate:
				CMP AH, 01h
				JNE MainLoop
	
	EndProgram:
		
		MOV     AH, 4Ch                			; These two instructions use a DOS interrupt
		INT     21h                    			; to release the memory for this program and
                                       			; then return control to DOS.  This is the
                                       			; logical end of the program.

Main	 ENDP                      			; End of the Main proc

;------------------------------------------------------------------------ END MAIN

;------------------------------------------------------------------------ ProcessKey
 ProcessKey	   PROC	
 	
 	PUSH SI CX BX
 	MOV AH, 10h
 	INT 16h					;AX contains key code for key to process

	LeftArrow:					;Is the key left arrow?
 		CMP AH, 4Bh
 		JNE RightArrow
 		CMP leftColumn, 0			;Is box at far left?
 		JA MoveLeft	
 		JMP EndOfProcess
 			MoveLeft:
 				MOV BX, StartPosition
 				SUB BX, 2			;Move left 1 column
 				MOV StartPosition, BX
 				JMP EndOfProcess
 	
 	RightArrow:
 		CMP AH, 4Dh				;Is the key right arrow?
 		JNE UpArrow
 		CMP rightColumn, 158			;Is box at far right?
 		JB MoveRight
 		JMP EndOfProcess
 			MoveRight:
  				MOV BX, StartPosition
 				ADD BX, 2			;Move right 1 column
 				MOV StartPosition, BX
 				JMP EndOfProcess		
 		
 	UpArrow:
 		CMP AH,48h				;Is the key up arrow?
 		JNE DownArrow
 		CMP topRow, 0				;Is box at top of screen?
 		JNE MoveUp
 		JMP EndOfProcess
 			MoveUp:
  				MOV BX, StartPosition
 				SUB BX, 160				;Move up 1 row
 				MOV StartPosition, BX
 				JMP EndOfProcess		
 	
 	DownArrow:
		CMP AH, 50h				;Is the key down arrow?
		JNE F1
		CMP bottomRow, 23			;Is box in bottom row
		JNE MoveDown
		JMP EndOfProcess
			MoveDown:
				MOV BX, StartPosition
				ADD BX, 160				;Move down 1 row
				MOV StartPosition, BX
 				JMP EndOfProcess

 	F1:	
 		CMP AH,3Bh				;Is the key F1?
 		JNE F2
 		MOV CH,0
 		CMP CH, borderSize			;Is border set to single line?
 		JNE SetSingle
 		MOV CH,1
 		MOV borderSize,CH			;Set borderSize to double line
 		JMP EndOfProcess
 		
 		SetSingle:
 			MOV borderSize, CH		;Set borderSize to single line
 			JMP EndOfProcess
 		
	F2:	
		CMP AH,3Ch				;Is the key F2?
		JNE F3
		LEA SI,colorArray
		MOV BL, foregroundPos
		CMP BL,3				;Is foregroundPos at end of array
		JL F2Increment
		MOV BL,0				;foregroundPos % 3
		MOV foregroundPos, BL	
		JMP EndOfF2
		
		F2Increment:
			INC BL				;Move to next color position
			MOV foregroundPos,BL
		
		EndOfF2:
			MOV BH, 0
			MOV CX,[SI + BX]
			MOV foregroundColor, CL		;Change foreground color
			JMP EndOfProcess
				
		
 	F3:	
 		CMP AH,3Dh				;Is the key F3?
 		JNE F6
 		LEA SI,colorArray
 		MOV BL, backgroundPos
 		CMP BL,3				;Is backgroundPos at end of array
 		JL F3Increment
 		MOV BL,0				;backgroundPos % 3
 		MOV backgroundPos, BL	
 		JMP EndOfF3
 		
 		F3Increment:
 			INC BL				;Move to next color position
 			MOV backgroundPos,BL
 		
 		EndOfF3:
 			MOV BH, 0
 			MOV CX, [SI + BX]
 			MOV backgroundColor,CL		;Change background color
			JMP EndOfProcess
			
	F6:
		CMP AH, 20h	;40h				;Is the key F6?
		JNE F7
		MOV BX, snowPercentage
		CMP BX, 10				;Is the percentage at the lowest?
		JGE DecreasePercentage
		JMP EndOfProcess
			DecreasePercentage:
				MOV ES:[4000 - 20], word ptr 4700h + 'D'
				DEC BX
				MOV snowPercentage, BX
				JMP EndOfProcess
			
	F7:
		CMP AH,2Eh	;41h				;Is the key F7?
		JNE F8
		MOV BX, snowPercentage
		CMP BX, 400				;Is the percentage at the highest?
		JLE IncreasePercentage
		JMP EndOfProcess
			IncreasePercentage:
				MOV ES:[4000 - 20], word ptr 4700h + 'C'
				INC BX
				MOV snowPercentage, BX
				JMP EndOfProcess
		
		
	F8:
		CMP AH, 30h	;42h				;Is the key F8?
		JNE F9
		MOV BX, snowSpeed
		CMP BX, 10				;Is the speed at the highest?
		JG DecreaseSpeed
		JMP EndOfProcess
			DecreaseSpeed:
				MOV ES:[4000 - 20], word ptr 4700h + 'B'
				DEC BX
				MOV snowSpeed, BX
				JMP EndOfProcess
			
	F9:
		CMP AH, 1Eh	;43h				;Is the key F8?
		JNE CtrlUp
		MOV BX, snowSpeed
		CMP BX, 200				;Is the speed at the lowest?
		JL IncreaseSpeed
		JMP EndOfProcess
			IncreaseSpeed:
				MOV ES:[4000 - 20], word ptr 4700h + 'A'
				INC BX
				MOV snowSpeed, BX
				JMP EndOfProcess
 		
 	CtrlUp:
		CMP AH,8Dh
		JNE CtrlDown				;Is the key Ctrl/up arrow?
		CMP boxHeight, 22			;Is the box at the max size
		JGE EndOfProcess
		CMP rightColumn, 156
		JB NotOnRight				;Is the box past the right edge?
		MOV BX, startPosition
		SUB BX, 4				;Move left
		MOV startPosition,BX
		NotOnRight:
			CMP bottomRow, 23		;Is the box on bottom row?
			JL NotOnBottom
			MOV BX, startPosition
			SUB BX, 160			;Move up a row
			MOV startPosition, BX
			NotOnBottom:
				CMP leftColumn, 0	;Is the box past the left edge?
				JG NotLeftEdge
					MOV BX, startPosition
					;ADD BX, 4	;Move right
					MOV startPosition,BX
					NotLeftEdge:
						MOV BH,0
						MOV BL, boxHeight
						ADD BL, 1
						MOV boxHeight, BL
 					JMP EndOfProcess
 	
 	CtrlDown:
		CMP AH,91h				;Is the key Ctrl/down arrow?
		JNE EndOfProcess
		CMP boxHeight, 6
		JLE EndOfProcess
		CMP leftColumn, 0
		JG NotOnLeft	
		NotOnLeft:
			MOV BL, boxHeight
			SUB BL, 1
			MOV boxHeight, BL 
 	
 	EndOfProcess:
 	CALL GetEdges
	POP BX CX SI
 	
 	RET			  			; Return to Main proc
	ProcessKey	   ENDP			  		; End of DrawBox		
;------------------------------------------------------------------------ END ProcessKey

;------------------------------------------------------------------------ DrawBox	

DrawBox	   PROC			
	
	PUSH AX BX CX DX SI DI
	CALL ClearScreen
	Call GetEdges					;Moves the startPosition if off screen	
	
	Draw:
		MOV DI,startPosition
		CMP borderSize, 0			;If border size should be single
		JG DoubleBorder	
		LEA SI, singleLine
		JMP BoxDrawing
		
	DoubleBorder:					;Else border size should be double					
		LEA SI, doubleLine
	
	BoxDrawing:
		MOV DH, backgroundColor
		SHL DH, 4				;Shifts background color into upper bits of DH
		OR DH, foregroundColor			;Move background color and foreground color into DH		
		MOV DL,[SI + UpperLeft]			
		MOV ES:[DI],DX				;Puts upper left corner on screen
		MOV DL,[SI + UpperRight]
		MOV BH, 0
		MOV BL, boxWidth			;BX = length of the box to be displayed
		MOV ES:[DI + BX],DX			;Puts upper right corner on screen
		MOV DL, [SI + LowerLeft]
		MOV AL, boxHeight
		MOV BL, 160				;Moves number of columns into BL
		MUL BL					;Multiplies boxHeight by number of columns
		MOV BX, AX
		PUSH BX					;Stores BX for HorizontalLoop
		MOV ES:[DI + BX],DX			;Puts lower left corner on screen
		MOV DL, [SI + LowerRight]
		MOV AL, boxWidth
		MOV AH, 0
		ADD BX, AX				;Add width of the box to the bottom left corner position
		MOV ES: [DI + BX], DX			;Puts lower right corner on screen
		MOV CL, boxWidth			;Counter to keep track of Horizontal position
		SUB CL,2				;Removes the last position from the width
		MOV CH, 0			
		POP BX					;Gets the BX that contained position for last row
		PUSH DI					;Stores DI that has initial start position
		
		HorizontalLoop:				;BX = beginning column of last row of box
		
			ADD DI,2
			MOV DL, [SI + HorizontalLine]	
			MOV ES:[DI], DX			;Puts horizontal line on top row
			MOV ES:[DI + BX], DX 		;Puts horizontal line on bottom row
			SUB CX, 2
			CMP CX, 0 			
			JG HorizontalLoop		;while loop iterations remaining > 0
		
		MOV CL, 1				;reset counter
		MOV CH, 0
		POP DI					;Remove DI that has initial start position
		MOV BH, 0
		MOV BL, boxWidth			;Move the width of box into BX
		MOV AL, boxHeight
		MOV AH, 0				;Move the height of box into AX
		
		MOV DL, [SI + VerticalLine]
		
		VerticalLoop:
			ADD DI, 160 				;Move to next row	
			MOV ES:[DI],DX				;Put vertical line on left side of screen
			MOV ES:[DI + BX],DX			;Put vertical line on right side of screen
			ADD CL, 1
			CMP CX,AX
			JL VerticalLoop			;while loop iterations < box height
	
	CALL GetBoxView
	POP DI SI DX CX BX AX
	RET			  			; Return to Main proc
	DrawBox	   ENDP			  		; End of DrawBox

;------------------------------------------------------------------------ END DrawBox

;------------------------------------------------------------------------ ClearScreen	

ClearScreen	   PROC	

	PUSH DI CX AX
	
	MOV DI, 0						;Make DI index for screen memory
	MOV CX, 4000 - 158					;CX contains number of times to loop for screen memory
	MOV AX, 0720h						;Make AX = a black space
	
	CLS:
		MOV ES:[DI], AX
		ADD DI, 2
		CMP DI, CX					;Is this the end of screen to clear?
		JL CLS
		
	
	POP AX CX DI
	
	RET			  				; Return to Main proc
	ClearScreen	   ENDP			  		; End of ClearScreen
	
;------------------------------------------------------------------------ END ClearScreen

;------------------------------------------------------------------------ CheckTime

CheckTime	   PROC	

	PUSH DX BX AX 
		
	MOV AH, 0
	INT 1Ah
	STC
	CMP DX, updateTime					;If current ticks > update time
	JL NotEnoughTime
		MOV BX, updateTime
		ADD BX, snowSpeed				;Set new update time to currentTicks + time between ticks
		MOV updateTime, BX
		CLC
		
		
	NotEnoughTime:
	POP AX BX DX 
	
	RET			  				; Return to Main proc
	CheckTime	   ENDP			  		; End of CheckTime
	
;------------------------------------------------------------------------ END CheckTime

;------------------------------------------------------------------------ UpdateSnow

UpdateSnow	   PROC	

	PUSH DI DX CX BX AX SI DS ES
	
	MOV CX, 2000 - 80
	LEA DI, snowScreen + 4000				;Get address of last position of snowScreen
	LEA SI, snowScreen + 4000 - 160				;Get address of next to last row of snowScreen
	PUSH DS
	POP ES 							;Make ES point to DS to swap rows
	STD
	REP MOVSW						;Swap top row into the line below in snowScreen
	
	POP ES DS SI AX BX CX DX DI
	
	RET			  				; Return to Main proc
	UpdateSnow	   ENDP			  		; End of UpdateSnow
	
;------------------------------------------------------------------------ END UpdateSnow

;------------------------------------------------------------------------ GetBoxView

GetBoxView	   PROC	

	PUSH DI DX CX BX AX SI
	
	MOV DI,0						;Set DI for index in screen memory
	MOV BL, leftColumn
	MOV CL, topRow
	CMP leftColumn, 0					
	
	MOV BH,0			
	MOV CH, 0
	MOV DX, -1						;Set DX to count row position to inside box
	MoveToTopRow:
		ADD DI, 160					;Move to next row
		ADD DX,1
		CMP CX,DX					;While not 1 row past top row of box
		JG MoveToTopRow
		
	MOV DX, -2						;Reset DX to count column position to box
	
	MoveToLeftColumn:
		ADD DI,2					;Move to next column
		ADD DX, 2
		CMP BX, DX					;While not at left column of box
		JG MoveToLeftColumn
	
	MOV CX, 0						
	MOV BX, 0
	MOV AX, DI						;Move snow start position into AX
	MOV BX, 1
	
	rowLoop:
		MOV CL, leftColumn				;Reset CL to the left column position
		ADD CL, 2					;Move position inside the box
		MOV CH, 0
		LEA SI, snowScreen
		ADD SI, AX					;Set SI to the next row inside the box
		MOV DI, AX					;Set DI to the next row inside the box
		ColumnLoop:
			MOV DX, DS:[SI]				;Get what is stored in equivalent position in snowScreen in DX
			MOV ES:[DI], DX				;Put what was in snowScreen on screen
			ADD DI, 2				;Move to the next column
			ADD SI, 2				;Move to the next column
			ADD CL, 2				;Move count to the next column
			CMP CL, rightColumn			;While still inside the box
			JB ColumnLoop
		ADD AX, 160					;Move to next row of screen memory
		ADD BX, 1					;Move to next row of loop
		CMP BL, boxHeight				;While still inside the box
		JL rowLoop
		
	POP SI AX BX CX DX DI
	
	RET			  				; Return to AddToScreen
	GetBoxView	   ENDP			  		; End of GetBoxView
	
;------------------------------------------------------------------------ END GetBoxView

;------------------------------------------------------------------------ AddNewRow

AddNewRow	   PROC	

	PUSH DI DX CX BX AX SI
	
	LEA SI,snowScreen					;Make SI point to the snowScreen
	MOV CX, 0
	SnowLoop:
		MOV BX, randomNumber
		MOV AX, multiplyByValue
		MUL BX						;AX contains random value
		MOV BX, addValue
		SUB AX, BX					;Add random value + constant
		MOV randomNumber, AX
		MOV DX, 0700h + ' '
		MOV [SI],DX					;Store a black space in snowScreen
		AND AX, 1111111111b
		CMP AX, snowPercentage
		JAE NoSnow
			MOV DX, 0700h + '*'			;Replace space with a * in snowScreen
			MOV [SI],DX
	NoSnow:
		INC SI
		INC SI
		ADD CX, 2
		CMP CX, 160					;Is this the end of the row?
		JL SnowLoop
	
	POP SI AX BX CX DX DI
	
	RET			  				; Return to Main proc
	AddNewRow	   ENDP			  		; End of AddNewRow
	
;------------------------------------------------------------------------ END AddNewRow

;------------------------------------------------------------------------ GetEdges
GetEdges	PROC

	PUSH AX BX CX 
	
	MOV AH, 0
	MOV BL, boxHeight			
	MOV AL, 2
	MUL BL					;Multiplies the box height * 4
	MOV boxWidth, AL			;Sets the box width to 4 * box height to take care of extra byte for color
	MOV BX, 0
	
	MOV AX, startPosition
	MOV BL, 160
	DIV BL					;AH contains the left column position
						;AL contains the row position of top of box
	MOV leftColumn, AH		
	ADD AH, boxWidth			;AH contains the right column position
	MOV BL, boxWidth
	ADD AH,BL
	MOV rightColumn,AH	
	MOV topRow,AL				
	ADD AL, boxHeight			;AL contains the row position + height of the box
	MOV bottomRow,AL
	
	MOV AL,boxWidth
	ADD boxWidth,AL
	
	POP CX BX AX
	
	RET			  				; Return to Main proc
	GetEdges	   ENDP			  		; End of GetEdges
	
;----------------------------------------------------------------------------------- End GetEdges

;----------------------------------------------------------------------------------- Count	
Count	PROC	
	
	PUSH AX BX DX DI
	
	MOV DX, 0
	MOV AX, timeCounter
	ADD AX, 1
	MOV timeCounter, AX
	MOV BX, 60000
	DIV BX
	CMP DX, 0
	JNE EndOfCount
	
	MOV AX, 1
	MOV timeCounter, AX
	MOV DI, 3998
	MOV BX, 10
	MOV AX, counter
	ADD AX, 1
	MOV counter, AX	
	
	CountLoop:
		MOV DX, 0
		DIV BX
		ADD DL, '0'
		MOV ES:[DI],DL
		SUB DI,2
		CMP AX, 0
		JNE CountLoop
	
	EndOfCount:
		POP DI DX BX AX
	
	RET			  				; Return to Main proc
	Count	   ENDP			  			; End of Count
			
;----------------------------------------------------------------------------------- End Count			

MyCode ENDS                        			; End Segment

;-------------------------------------------------------------------------------------
END Main                           			; This END marks the physical end of the source code
                                   			; file.  The label specified will be the
                                   			; "user transfer address" where execution will begin.